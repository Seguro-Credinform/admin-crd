/**
 * Created by Desarrollo KJ on 22/2/2017.
 */
import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";
import {RouterModule} from "@angular/router";
import {routes} from './routing';

import {User} from './component';
import {Login} from './subpages/login';
import {Register} from './subpages/register';
import { ToastrModule,  } from 'ngx-toastr';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
    ToastrModule.forRoot()
  ],
  providers : [
  ],
  declarations: [
    User,
    Login,
    Register
  ]
})
export class UserModule {
  public static routes = routes;
}
