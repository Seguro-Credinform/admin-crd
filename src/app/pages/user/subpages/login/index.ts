/**
 * Created by Desarrollo KJ on 23/8/2017.
 */
import {Component} from "@angular/core";
import {ToastrService} from 'ngx-toastr';
import {UsuarioService} from './../../../../services';
import {Router} from "@angular/router";

@Component({
  selector: 'login',
  templateUrl: 'index.html',
  styleUrls: ['index.css']
})

export class Login {

  public formUser: any = {};
  public submitted: boolean = false;


  constructor(private _t: ToastrService, private _service: UsuarioService, private _routes: Router) {
    _service.logout();
  }

  public onSubmit(): void {
    this.submitted = true;
    this._service.login({email: this.formUser.email, password: this.formUser.password}).subscribe(
      result => {
        this._t.success('Exito, espere unos segundos');

        if (localStorage.getItem('tipo') == '2') {
          this._routes.navigateByUrl('reporte-siniestro');
        }
        if (localStorage.getItem('tipo') == '3') {
          console.log('hola usuario');
          this._routes.navigateByUrl('buzon-sugerencia');
        }
        this._routes.navigateByUrl('reporte-siniestro');
        this.submitted = false;
      },
      error => {
        let msg = JSON.parse(error._body);
        this._t.error(msg.message);
        this.submitted = false;
      }
    );
  }

}
