import {Login} from './subpages/login';
import {Register} from './subpages/register';

export const routes = [
  { path: '', children: [
    { path: 'login', component: Login },
    { path: 'register', component: Register }
  ]},
];

