/**
 * Created by Desarrollo KJ on 22/2/2017.
 */
import {NgModule} from "@angular/core";
import {SharedModule} from './../../shared.module';
import {RouterModule} from "@angular/router";
import {routes} from './routing';

import {Home} from './component';
import {BuzonSugerencia} from './subpages/buzonSugerencia';
import {ServiciosAdicionales} from './subpages/servicios';
import {Chat} from './subpages/chat';
import {ReporteSiniestro} from './subpages/reporteSiniestro';
import {MapaComponent} from './subpages/mapa';
import {PreguntasFrecuentesComponent} from './subpages/preguntasFrecuentes';
import {PasswordComponent} from './subpages/password';
import {PaginationModule, TabsModule} from 'ngx-bootstrap';

import {NguiMapModule} from '@ngui/map';
import {ToastrModule} from 'ngx-toastr';

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(routes),
    PaginationModule.forRoot(),
    ToastrModule.forRoot(),
    TabsModule.forRoot(),
    NguiMapModule.forRoot({apiUrl: 'https://maps.googleapis.com/maps/api/js?key=AIzaSyBDt5B4nn-v1OFfMY06Sfi0cMYHxOKHuF8'})
  ],
  providers : [
  ],
  declarations: [
    Home,
    BuzonSugerencia,
    ServiciosAdicionales,
    Chat,
    ReporteSiniestro,
    MapaComponent,
    PreguntasFrecuentesComponent,
    PasswordComponent
  ]
})
export class HomeModule {
  public static routes = routes;
}
