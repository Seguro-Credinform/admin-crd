/**
 * Created by Desarrollo KJ on 23/8/2017.
 */
import {Component} from "@angular/core";
import {ServiciosAdicionalesService, UsuarioService} from './../../../../services';
import {Observable} from 'rxjs/Rx';
import {ToastrService} from 'ngx-toastr';
import {Router} from "@angular/router";

declare var google: any;
declare var navigator: any;


@Component({
  selector: 'servicios-adicionales',
  templateUrl: 'index.html',
  styleUrls: ['index.scss']
})

export class ServiciosAdicionales {
  public list: any = [];
  public option: any = {
    perPage: 10,
    page: 1,
    sort: 'fechaCreacion:-1'
  };

  public optionMaps:any={
    center:'0, 0',
    draggable:true,
    zoom:15
  }

  public optionMarket:any={
    position:[]
  }

  public totalItems: number = 0;
  public itemsPerPage: number = 10;
  public currentPage: number = 1;

  public status: number = -1;

  public item: any = {
    _id: '',
    usuario: {
      Usuario: {
        Nombre: ''
      }
    },
    latitud:'',
    longitud:'',
    servicioAdicional: {
      nombre: ''
    },
    fechaCreacion: "",
    status: 1
  }

  public control: boolean = false;


  constructor(private _s: ServiciosAdicionalesService, private _t:ToastrService, private _u:UsuarioService, private _router:Router) {
    console.log('service');
    if(!localStorage.getItem('login')){
      _u.logout();
      _router.navigateByUrl('user/login');
    }
    else {
      console.log(localStorage.getItem('tipo'));
      if(localStorage.getItem('tipo') == '3'){
        console.log('hola 3');
        _router.navigateByUrl('buzon-sugerencia');
      }
    }

    this.getAll();
    Observable.interval(500 * 60).subscribe(x => {
      this.getAll();
    });
  };

  public getAll() {
    this._s.getAllSolicitudes(this.option).subscribe(
      (res) => {
        this.list = res.json();
        console.log(this.list);
        this.totalItems = res.headers.get('X-Total-Count');
      },
      (err) => {
        this.list.length = 0;
        console.log(err);
      }
    );
  };


  public edit(item) {
    this.control = true;
    this.item = item;
    this.optionMaps.center = this.item.latitud+', '+this.item.longitud;
    this.optionMarket.position = [parseFloat(this.item.latitud), parseFloat(this.item.longitud)];
  }


  public exit() {
    this.control = false;
    this.item = {
      _id: '',
      usuario: {
        Usuario: {
          Nombre: ''
        }
      },
      latitud:'',
      longitud:'',
      servicioAdicional: {
        nombre: ''
      },
      fechaCreacion: "",
      status: 1
    }
  }

  public saveAtendido() {

    let data = {
      _id: this.item._id,
      status: 2
    }

    this._s.editSolicitudes(data).subscribe(
      (res) => {
        this.item.status = 2;
        this._t.success('Exito en el guardado');
        this.getAll();
      }
    );
  }

  public saveIgnorado() {

    let data = {
      _id: this.item._id,
      status: 3
    }

    this._s.editSolicitudes(data).subscribe(
      (res) => {
        this._t.success('Exito en el guardado');
        this.item.status = 3;
        this.getAll();
      }
    );
  }

  public pageChanged(event) {
    this.option.page = event.page;
    this.getAll();
  }

  public filter(): void {
    if (this.status > 0) {
      this.option.status = this.status;
    }
    else {
      delete this.option.status;
    }

    this.getAll();
  }


}
