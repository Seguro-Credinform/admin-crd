/**
 * Created by Desarrollo KJ on 23/8/2017.
 */
import {Component} from "@angular/core";
import {SiniestroService, UsuarioService} from './../../../../services';
import {Observable} from 'rxjs/Rx';
import { Router} from "@angular/router";
import { ToastrService } from 'ngx-toastr';

declare var google: any;
declare var navigator: any;


@Component({
  selector: 'servicios-adicionales',
  templateUrl: 'index.html',
  styleUrls: ['index.scss']
})

export class ReporteSiniestro {
  public list: any = [];
  public list2: any = [];
  public option: any = {
    perPage: 10,
    page: 1,
    sort: 'fechaCreacion:-1',
    status:2
  };
  public option2: any = {
    perPage: 10,
    page: 1,
    sort: 'fechaCreacion:-1',
    status:3
  };

  public optionMaps:any={
    center:'0, 0',
    draggable:true,
    zoom:15
  }

  public optionMarket:any={
    position:[]
  }

  public totalItems: number = 0;
  public itemsPerPage: number = 10;
  public currentPage: number = 1;

  public totalItems2: number = 0;
  public itemsPerPage2: number = 10;
  public currentPage2: number = 1;

  public status: number = -1;

  public item: any = {
    _id: '',
    usuario: {
      Usuario: {
        Nombre: ''
      },
      listaPolizas:[]
    },
    descripcion:'',
    latitud:'',
    longitud:'',
    servicioAdicional: {
      nombre: ''
    },
    fotos:[],
    fechaCreacion: "",
    status: 1
  }

  public control: boolean = false;


  constructor(private _s: SiniestroService, private _u:UsuarioService, private _router:Router, private _t:ToastrService) {
    console.log(localStorage.getItem('login'));
    if(!localStorage.getItem('login')){
      _u.logout();
      _router.navigateByUrl('user/login');
    }
    else {
      console.log(localStorage.getItem('tipo'));
      if(localStorage.getItem('tipo') == '3'){
        console.log('hola 3');
        _router.navigateByUrl('buzon-sugerencia');
      }
    }

    this.getAll();
    Observable.interval(500 * 60).subscribe(x => {
      this.getAll();
    });
  };

  public getAll() {
    this._s.getAll(this.option).subscribe(
      (res) => {
        this.list = res.json();
        console.log(this.list);
      },
      (err) => {
        this.list.length = 0;
        console.log(err);
      }
    );

    this._s.getAll(this.option2).subscribe(
      (res) => {
        this.list2 = res.json();
        console.log(this.list2);
      },
      (err) => {
        this.list2.length = 0;
        console.log(err);
      }
    );
  };


  public edit(item) {
    this.control = true;
    this.item = item;
    this.optionMaps.center = this.item.latitud+', '+this.item.longitud;
    this.optionMarket.position = [parseFloat(this.item.latitud), parseFloat(this.item.longitud)];
  }


  public exit() {
    this.control = false;
    this.item = {
      _id: '',
      usuario: {
        Usuario: {
          Nombre: ''
        }
      },
      descripcion:'',
      latitud:'',
      longitud:'',
      servicioAdicional: {
        nombre: ''
      },
      fechaCreacion: "",
      status: 1
    }
      this.optionMaps={
        center:'0, 0',
        draggable:true,
        zoom:15
      }

  }

  public saveAtendido() {

    let data = {
      _id: this.item._id,
      status: 3
    }

    this._s.edit(data).subscribe(
      (res) => {
        this.item.status = 3;
        this._t.success('Guardado con exito');
        this.getAll();
      }
    );
  }


  public pageChanged(event) {
    this.option.page = event.page;
    this.getAll();
  }

  public pageChanged2(event) {
    this.option2.page = event.page;
    this.getAll();
  }

  public filter(): void {
    if (this.status > 0) {
      this.option.status = this.status;
    }
    else {
      delete this.option.status;
    }

    this.getAll();
  }


}
