/**
 * Created by Desarrollo KJ on 23/8/2017.
 */
import {Component, AfterViewChecked, ElementRef, ViewChild, OnInit} from "@angular/core";
import {ChatsService, UsuarioService} from './../../../../services';
import { Router} from "@angular/router";
import { ToastrService } from 'ngx-toastr';
import {Observable} from 'rxjs/Rx';
import { Socket } from 'ngx-socket-io';
declare var $;

@Component({
  selector: 'chat-component',
  templateUrl: 'index.html',
  styleUrls: ['index.scss']
})

export class Chat {
  public list: any = [];
  public mensaje: any = [];
  public status:number = -1;
  public mensajeEnviar:string = ''
  public tipoSugerencia:number = -1;
  @ViewChild('msjChat') private myScrollContainer: ElementRef;

  public option: any = {
    perPage: 99,
    page: 1,
    sort: 'status:1'
  };

  public chat: any = {
    _id:'',
    usuario:{
      Usuario: {
        Nombre:'',
        FechaNacimiento:'',
        Direccion:'',
        Correo:'',
        NumeroAsegurado:''
      },
      listaPolizas:[],
      telefono:''
    },
    tipoSugerencia: 0,
    fechaCreacion: "",
    status: 0,
    respuesta: "",
    chat: "",
    asunto: ""
  }


  constructor(private _s: ChatsService, private _t:ToastrService, private _u:UsuarioService, private _router:Router, private socket: Socket) {

    if(!localStorage.getItem('login')){
      _u.logout();
      _router.navigateByUrl('user/login');
    }
    else {
      console.log(localStorage.getItem('tipo'));
      if(localStorage.getItem('tipo') == '3'){
        console.log('hola 3');
        _router.navigateByUrl('buzon-sugerencia');
      }
    }

    this.getAll();

    Observable.interval(500 * 60).subscribe(x => {
      this.getAll();
    });

    this.socket.on('connect', (res)=>{
      console.log('conectado');
    });

    this.socket.on('conectados', (res)=>{
      console.log(res);
    });
  };

  public getAll() {
    this._s.getAll(this.option).subscribe(
      (res) => {
        this.list.length = 0;
        this.list = res.json();
      },
      (err) => {
        this.list.length = 0;
      }
    );
  };


  public edit(item){
    this.chat = item;
    this.socket.emit('login',JSON.stringify({chat:item._id, emisor:2}));
    this._s.getAllMensaje(item._id).subscribe(
      res=>{
        this.mensaje.length = 0;
        this.mensaje = res.json();
      },
      err=>{
        this.mensaje.length = 0;
      }
    );

    this.socket.on('respuesta', (data)=>{
      this.mensaje.push(JSON.parse(data));
    })
  }

  public enviarMsj(){
    if(!this.mensajeEnviar){
      return false
    }

    this.socket.emit('mensaje', this.mensajeEnviar);
    this.mensajeEnviar = '';
  }

  public save(){
    if(!this.chat.chat){
      alert('Se requiere de una respuesta');
      return false;
    }

    let data = {
      _id:this.chat._id,
      respuesta:this.chat.respuesta,
      status:2
    }

    this._s.edit(data).subscribe(
      res=>{
        this._t.success('Enviado con exito');
        this.getAll();
      }
    );
  }

  public filter(): void {
    if (this.status > 0) {
      this.option.status = this.status;
    }
    else {
      delete this.option.status;
    }

    if (this.tipoSugerencia > 0) {
      this.option.tipoSugerencia = this.tipoSugerencia;
    }
    else {
      delete this.option.tipoSugerencia;
    }

    this.getAll();
  }

  public saveAtendido() {

    let data = {
      _id: this.chat._id,
      status: 2
    }

    this._s.edit(data).subscribe(
      (res) => {
        this.chat.status = 2;
        this._t.success('Exito en el guardado');
        this.getAll();
      }
    );
  }



}
