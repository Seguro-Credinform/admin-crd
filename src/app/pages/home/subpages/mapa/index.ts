/**
 * Created by Desarrollo KJ on 23/8/2017.
 */
import {Component} from "@angular/core";
import {Mapas, UsuarioService} from './../../../../services';
import {ToastrService} from 'ngx-toastr';
import {Router} from "@angular/router";

declare var google: any;
declare var navigator: any;


@Component({
  selector: 'mapa-component',
  templateUrl: 'index.html',
  styleUrls: ['index.scss']
})

export class MapaComponent {
  public list: any = [];
  public option: any = {
    perPage: 10,
    page: 1,
    sort: 'fechaCreacion:-1'
  };

  public optionMaps:any={
    center:'0, 0',
    draggable:true,
    zoom:15
  }

  public optionMarket:any={
    position:[]
  }

  public totalItems: number = 0;
  public itemsPerPage: number = 10;
  public currentPage: number = 1;

  public tipo: number = -1;

  public item: any = {
    _id: '',
    nombre:'',
    latitud:'',
    longitud:'',
    direccion: "",
    status: 1,
    tipoUbicacion: 1,
    telefonos:[''],
    correo:[''],
    horario:''
  };

  public control: boolean = false;


  constructor(private _s: Mapas, private _t:ToastrService, private _u:UsuarioService, private _router:Router) {
    console.log('service');
    if(!localStorage.getItem('login')){
      _u.logout();
      _router.navigateByUrl('user/login');
    }
    else {
      if(localStorage.getItem('tipo') != '1'){
        _router.navigateByUrl('');
      }
    }
    this.getAll();
  };

  public getAll() {
    this._s.getAll(this.option).subscribe(
      (res) => {
        this.list = res.json();
        console.log(this.list);
        this.totalItems = res.headers.get('X-Total-Count');
      },
      (err) => {
        this.list.length = 0;
        console.log(err);
      }
    );
  };


  public nuevo(){
    this.control = true;
    this.item = {
      _id: '',
      nombre:'',
      latitud:'',
      longitud:'',
      direccion: "",
      status: 1,
      tipoUbicacion: 1,
      telefonos:[''],
      correo:[''],
      horario:''
    };

    this.optionMaps.center = '-16.4882197, -68.1202839';
    this.optionMarket.position = [-16.4882197, -68.1202839];
  }

  public edit(item) {
    this.control = true;
    this.item = {
      _id: item._id,
      nombre:item.nombre,
      latitud:item.latitud,
      longitud:item.longitud,
      direccion:item.direccion,
      status:item.status,
      tipoUbicacion:item.tipoUbicacion,
      telefonos:[],
      correo:[],
      horario:item.horario
    };

    this.item.telefonos.push(item.telefonos[0])
    this.item.correo.push(item.correo[0])

    console.log(item);

    this.optionMaps.center = this.item.latitud+', '+this.item.longitud;
    this.optionMarket.position = [parseFloat(this.item.latitud), parseFloat(this.item.longitud)];
  }


  public cancelar() {
    this.control = false;
    this.item = {
      _id: '',
      nombre:'',
      latitud:'',
      longitud:'',
      direccion: "",
      status: 1,
      tipoUbicacion: 1,
      telefonos:[''],
      correo:[''],
      horario:''
    }
  }



  public pageChanged(event) {
    this.option.page = event.page;
    this.getAll();
  }

  public filter(): void {
    if (this.tipo > 0) {
      this.option.tipoUbicacion = this.tipo;
    }
    else {
      delete this.option.tipoUbicacion;
    }

    this.getAll();
  }

  onMapClick(event){
    this.item.latitud = event.latLng.lat();
    this.item.longitud = event.latLng.lng();
    console.log(this.item.latitud);
    console.log(this.item.longitud);
    this.optionMarket.position = event.latLng;
  }

  public guardar(): void {
    if (this.item._id != '') {
      this._s.edit(this.item).subscribe(
        (result) => {
          this._t.success('Exito en el guardado');
          this.getAll();
        },
        (error) => {
          this._t.error('Error en el guardado');
        }
      );
    }
    else {
      this._s.save(this.item).subscribe(
        (result) => {
          this._t.success('Exito en el guardado');
          this.getAll();
          this.cancelar();
        },
        (error) => {
          this._t.error('Error en el guardado');
        }
      );
    }
  }


}
