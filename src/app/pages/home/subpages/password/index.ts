/**
 * Created by Desarrollo KJ on 23/8/2017.
 */
import {Component} from "@angular/core";
import {ToastrService} from 'ngx-toastr';
import {UsuarioService} from './../../../../services';
import {Router} from "@angular/router";

@Component({
  selector: 'password-component',
  templateUrl: 'index.html',
  styleUrls: ['index.css']
})

export class PasswordComponent {

  public formUser: any = {};
  public submitted: boolean = false;


  constructor(private _t: ToastrService, private _service: UsuarioService, private _router:Router) {
    if(!localStorage.getItem('login')){
      _service.logout();
      _router.navigateByUrl('user/login');
    }
  }

  public onSubmit(): void {
    this.submitted = true;
    this._service.edit({_id: localStorage.getItem('idUsuario'), password: this.formUser.password}).subscribe(
      result => {
        this._t.success('Exito, su clave ha sido cambiada');
        this.submitted = false;
      },
      error => {
        let msg = JSON.parse(error._body);
        this._t.error(msg.message);
        this.submitted = false;
      }
    );
  }

}
