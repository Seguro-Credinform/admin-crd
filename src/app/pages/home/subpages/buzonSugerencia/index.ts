/**
 * Created by Desarrollo KJ on 23/8/2017.
 */
import {Component} from "@angular/core";
import {SugerenciasService, UsuarioService} from './../../../../services';
import { Router} from "@angular/router";
import { ToastrService } from 'ngx-toastr';
import {Observable} from 'rxjs/Rx';

@Component({
  selector: 'buzon-sugerencia',
  templateUrl: 'index.html',
  styleUrls: ['index.scss']
})

export class BuzonSugerencia {
  public list: any = [];
  public status:number = -1;
  public tipoSugerencia:number = -1;

  public option: any = {
    perPage: 99,
    page: 1,
    sort: 'fechaCreacion:-1'
  };

  public mensaje: any = {
    _id:'',
    usuario:{
      Usuario: {
        Nombre:''
      }
    },
    tipoSugerencia: 0,
    fechaCreacion: "",
    status: 1,
    respuesta: "",
    mensaje: "",
    asunto: ""
  }


  constructor(private _s: SugerenciasService, private _t:ToastrService, private _u:UsuarioService, private _router:Router) {

    if(!localStorage.getItem('login')){
      _u.logout();
      _router.navigateByUrl('user/login');
    }



    this.getAll();

    Observable.interval(300 * 60).subscribe(x => {
      this.getAll();
    });
  };

  public getAll() {
    this._s.getAll(this.option).subscribe(
      (res) => {
        this.list = res.json();
        console.log(this.list);
      },
      (err) => {
        this.list.length = 0;
        console.log(err);
      }
    );
  };


  public edit(item){
    this.mensaje = item;
  }

  public save(){
    if(!this.mensaje.mensaje){
      alert('Se requiere de una respuesta');
      return false;
    }

    let data = {
      _id:this.mensaje._id,
      respuesta:this.mensaje.respuesta,
      status:2
    }

    this._s.edit(data).subscribe(
      res=>{
        this._t.success('Enviado con exito');
        this.getAll();
      }
    );
  }

  public filter(): void {
    if (this.status > 0) {
      this.option.status = this.status;
    }
    else {
      delete this.option.status;
    }

    if (this.tipoSugerencia > 0) {
      this.option.tipoSugerencia = this.tipoSugerencia;
    }
    else {
      delete this.option.tipoSugerencia;
    }

    this.getAll();
  }
}
