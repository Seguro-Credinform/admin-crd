import {Home} from './component';
import {BuzonSugerencia} from './subpages/buzonSugerencia';
import {Chat} from './subpages/chat';
import {ServiciosAdicionales} from './subpages/servicios';
import {ReporteSiniestro} from './subpages/reporteSiniestro';
import {MapaComponent} from './subpages/mapa';
import {PreguntasFrecuentesComponent} from './subpages/preguntasFrecuentes';
import {PasswordComponent} from './subpages/password';

export const routes = [
  { path: '', children: [
    { path: '', component: ReporteSiniestro },
    { path: 'buzon-sugerencia', component: BuzonSugerencia },
    { path: 'chat', component: Chat },
    { path: 'servicios-adicionales', component: ServiciosAdicionales },
    { path: 'reporte-siniestro', component: ReporteSiniestro },
    { path: 'mapa', component: MapaComponent },
    { path: 'preguntas-frecuentes', component: PreguntasFrecuentesComponent },
    { path: 'password', component: PasswordComponent }
  ]},
];

