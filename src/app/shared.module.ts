import {NgModule}            from '@angular/core';
import {CommonModule}        from '@angular/common';
import {FormsModule}         from '@angular/forms';
import { HttpModule } from "@angular/http";


import {MdSliderModule, MdSnackBarModule} from '@angular/material';



import {Menu, RelatedSideMenu, SidebarComponent} from './components';
import {Configuration, SugerenciasService, ServiciosAdicionalesService, SiniestroService, UsuarioService, ChatsService, Mapas, PreguntasFrecuentesServices} from './services';



@NgModule({
  imports: [CommonModule, HttpModule  ],
  declarations: [Menu, RelatedSideMenu, SidebarComponent],
  exports: [Menu, RelatedSideMenu, SidebarComponent,
    CommonModule, FormsModule,HttpModule, MdSliderModule, MdSnackBarModule],
  providers:[Configuration, SugerenciasService, ServiciosAdicionalesService, SiniestroService, UsuarioService, ChatsService, Mapas, PreguntasFrecuentesServices]
})
export class SharedModule {
}
