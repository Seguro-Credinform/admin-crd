import { Component } from '@angular/core';
import {Routes, Router} from "@angular/router";

@Component({
  selector: 'app-root',
  template: '<router-outlet></router-outlet>',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'app';


  constructor() {

  }

}
