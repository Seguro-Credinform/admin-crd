import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { SharedModule } from './shared.module';
import { RouterModule, PreloadAllModules } from "@angular/router";

import { AppComponent } from './app.component';
import { Footer, Menu } from './components';
import { ROUTES } from "./app.routes";

import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';
const config: SocketIoConfig = { url: 'http://admincrd.com:8000', options: {} };
import { enableProdMode } from '@angular/core';

@NgModule({
  declarations: [
    AppComponent,
    Footer
  ],
  imports: [
    BrowserAnimationsModule,
    SharedModule,
    RouterModule.forRoot(ROUTES, { useHash: false, preloadingStrategy: PreloadAllModules }),
    SocketIoModule.forRoot(config)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
