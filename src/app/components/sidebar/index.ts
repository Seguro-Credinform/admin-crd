/**
 * Created by Desarrollo KJ on 23/8/2017.
 */
import {Component} from "@angular/core";
import {ServiciosAdicionalesService, SugerenciasService, SiniestroService, ChatsService} from "./../../services";
import {Observable} from 'rxjs/Rx';
import {Router} from "@angular/router";

@Component({
  selector: 'component-sidebar',
  templateUrl: 'index.html',
  styleUrls: ['index.scss']
})
export class SidebarComponent {

  public totalSugerencias: number = 0;
  public totalReportes: number = 0;
  public totalServicios: number = 0;
  public totalChat: number = 0;

  public tipoUser: number = 0;

  constructor(private _s: ServiciosAdicionalesService, private _su: SugerenciasService, private _si:SiniestroService, private _c:ChatsService, private _routes: Router) {
    console.log('service');
    this.getAll();
    Observable.interval(500 * 60).subscribe(x => {
      this.getAll();
    });

    if(localStorage.getItem('login')){
      this.tipoUser = parseInt(localStorage.getItem('tipo'));
    }


  };

  public getAll() {
    this._s.getAllSolicitudes({status: 1, perPage: 0}).subscribe(
      res => {
        this.totalServicios = res.headers.get('X-Total-Count');
      },
      err =>{
        this.totalServicios = 0;
      }
    );
    this._su.getAll({status: 1, perPage: 0}).subscribe(
      res => {
        this.totalSugerencias = res.headers.get('X-Total-Count');
      },
      err =>{
        this.totalSugerencias = 0;
      }
    );

    this._si.getAll({status: 2, perPage: 0}).subscribe(
      res => {
        this.totalReportes = res.headers.get('X-Total-Count');
      },
      err =>{
        this.totalReportes = 0;
      }
    );

    this._c.getAll({status: 1, perPage: 0}).subscribe(
      res => {
        this.totalChat = res.headers.get('X-Total-Count');
      },
      err =>{
        this.totalChat = 0;
      }
    );
  }

  public ruteo(ruta:string){
    this._routes.navigateByUrl(ruta);
  }
}


