  /**
 * Created by Desarrollo KJ on 23/8/2017.
 */
import {Component} from "@angular/core";
  import {UsuarioService} from './../../services';
  import { Router} from "@angular/router";


@Component({
  selector: 'menu-component',
  templateUrl: 'index.html',
  styleUrls:['index.scss']
})

export class Menu{
  public nombre:string = '';

  constructor(private  _u:UsuarioService, private _router:Router){
    this.nombre = localStorage.getItem('nombre');
  }

  public cerrarSesion(){
    this._u.logout();
    this._router.navigateByUrl('user/login');
  }
}
