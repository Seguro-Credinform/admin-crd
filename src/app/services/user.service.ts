import {Injectable} from "@angular/core";
import {Http, Response} from "@angular/http";
import "rxjs/add/operator/map";
import {Observable} from "rxjs/Observable";
import {Configuration} from "./config.service";
import * as _ from "lodash";

@Injectable()
export class UsuarioService {

  private _actionUrl: string;
  private _configuration;

  constructor(private _http: Http) {
    this._configuration = new Configuration();
    this._actionUrl = this._configuration.ServerWithApiUrl + 'usuarios/';
  }

  getAll(options): any {
    let params = new URLSearchParams();
    for(let key in options) {
      params.set(key, options[key])
    }
    return this._http.get(this._actionUrl+'?'+ params.toString(), this._configuration.resquestOption)
      .map(res => res);
  }


  save(data):any {
    let dataCopy = _.cloneDeep(data);
    delete dataCopy._id;
    return this._http.post(this._actionUrl, dataCopy, this._configuration.resquestOption).map(res => res.json());
  }

  edit(data):any {
    let dataCopy = _.cloneDeep(data);
    let id = data._id;
    delete dataCopy._id;
    return this._http.put(this._actionUrl + id, dataCopy, this._configuration.resquestOption).map(res => res.json());
  }

  delete(data):any {
    let id = data._id;
    return this._http.delete(this._actionUrl+id, this._configuration.resquestOption).map(res => res.json());
  }

  login(login:any){
    return this._http.post(this._actionUrl + 'login', login, this._configuration.resquestOption)
      .map((response:any) => {
        console.log(response);
        let user = JSON.parse(response._body);
        if (user && user.token) {
          localStorage.setItem('login', JSON.stringify(user));
          localStorage.setItem('token', user.token);
          localStorage.setItem('idUsuario', user._id);
          localStorage.setItem('nombre', user.nombre);
          localStorage.setItem('tipo', user.tipo);
        }
      });
  }

  logout() {
    localStorage.removeItem('login');
    localStorage.removeItem('token');
    localStorage.removeItem('idUsuario');
    localStorage.removeItem('nombre');
    localStorage.removeItem('tipo');
  }

}
