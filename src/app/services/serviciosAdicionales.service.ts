import {Injectable} from '@angular/core';
import {URLSearchParams, Http, Response} from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';
import {Configuration} from './config.service';
import * as _ from "lodash";

@Injectable()
export class ServiciosAdicionalesService {

  private _actionUrl: string;
  private _actionUrlBase: string;
  private _configuration;

  constructor(private _http: Http) {
    this._configuration = new Configuration();
    this._actionUrl = this._configuration.ServerWithApiUrl + 'serviciosAdicionalesSolicitudes/';
    this._actionUrlBase = this._configuration.ServerWithApiUrl + 'serviciosAdicionales/';
  }

  getAllSolicitudes(options): any {
    let params = new URLSearchParams();
    for(let key in options) {
      params.set(key, options[key])
    }
    return this._http.get(this._actionUrl+'?'+ params.toString(), this._configuration.resquestOption)
      .map(res => res);
  }


  saveSolicitudes(data):any {
    let dataCopy = _.cloneDeep(data);
    delete dataCopy._id;
    return this._http.post(this._actionUrl, dataCopy, this._configuration.resquestOption).map(res => res.json());
  }

  editSolicitudes(data):any {
    let dataCopy = _.cloneDeep(data);
    let id = data._id;
    delete dataCopy._id;
    return this._http.put(this._actionUrl + id, dataCopy, this._configuration.resquestOption).map(res => res.json());
  }

  deleteSolicitudes(data):any {
    let id = data._id;
    return this._http.delete(this._actionUrl+id, this._configuration.resquestOption).map(res => res.json());
  }

}
