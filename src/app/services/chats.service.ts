import {Injectable} from '@angular/core';
import {URLSearchParams, Http, Response} from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';
import {Configuration} from './config.service';
import * as _ from "lodash";
import { Socket } from 'ngx-socket-io';


@Injectable()
export class ChatsService {

  private _actionUrl: string;
  private _configuration;

  constructor(private _http: Http, private socket: Socket) {
    this._configuration = new Configuration();
    this._actionUrl = this._configuration.ServerWithApiUrl + 'chats/';
  }

  getAll(options): any {
    let params = new URLSearchParams();
    for(let key in options) {
      params.set(key, options[key])
    }
    return this._http.get(this._actionUrl+'?'+ params.toString(), this._configuration.resquestOption)
      .map(res => res);
  }

  getAllMensaje(chatId): any {
    return this._http.get(this._actionUrl+ chatId+'/mensajes?perPage=9999', this._configuration.resquestOption)
      .map(res => res);
  }


  save(data):any {
    let dataCopy = _.cloneDeep(data);
    delete dataCopy._id;
    return this._http.post(this._actionUrl, dataCopy, this._configuration.resquestOption).map(res => res.json());
  }

  edit(data):any {
    let dataCopy = _.cloneDeep(data);
    let id = data._id;
    delete dataCopy._id;
    return this._http.put(this._actionUrl + id, dataCopy, this._configuration.resquestOption).map(res => res.json());
  }

  delete(data):any {
    let id = data._id;
    return this._http.delete(this._actionUrl+id, this._configuration.resquestOption).map(res => res.json());
  }

  sendMessage(msg: string){
    this.socket.emit("message", msg);
  }

  getMessage() {
    return this.socket
      .fromEvent("message")
      .map( data => data );
  }

  getConnect(){
    return this.socket.on('connect', (res)=>{
      return (res);
    });
  }


  getLogin(id){
    return this.socket.emit('login', {chat:id, emisor:2});
  }

}
