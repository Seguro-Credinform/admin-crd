import {Injectable} from "@angular/core";
import {RequestOptions, Headers} from "@angular/http";

@Injectable()
export class Configuration {
  public Server: string;
  public ApiUrl: string;
  public ServerWithApiUrl: string;
  public customHeader:any;
  public header: Headers;
  public resquestOption: RequestOptions;

  constructor() {
    this.Server = 'http://104.236.187.54:8445/';
    this.ApiUrl = 'v1/';
    this.ServerWithApiUrl = this.Server + this.ApiUrl;
    this.customHeader = {
      'x-access-llave':'123456789'
    }
    this.header = new Headers();
    this.header.append('x-access-llave', this.customHeader['x-access-llave']);

    if (localStorage.getItem('token')) {
      this.customHeader['x-access-token'] = localStorage.getItem('token');
      this.header.append('x-access-token', localStorage.getItem('token'));
    }

    this.resquestOption = new RequestOptions({headers: this.header});
  }
}
